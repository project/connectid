<?php
/**
 * Implements hook_post_update_NAME() on Module connectid Post Update remove_config_from_openid_connect_integration.
 */
function connectid_post_update_remove_config_from_openid_connect_integration() {
  \Drupal::configFactory()
    ->getEditable('openid_connect.settings.connectid')
    ->delete();
}

