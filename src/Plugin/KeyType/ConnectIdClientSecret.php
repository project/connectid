<?php

namespace Drupal\connectid\Plugin\KeyType;

use Drupal\key\Plugin\KeyType\AuthenticationKeyType;

/**
 * Defines a key type for ConnectID Client Secret.
 *
 * @KeyType(
 *   id = "connect_id_client_secret",
 *   label = @Translation("ConnectID Client Secret"),
 *   description = @Translation("OAuth Client Secret for ConnectID."),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "text_field"
 *   }
 * )
 */
class ConnectIdClientSecret extends AuthenticationKeyType {}
