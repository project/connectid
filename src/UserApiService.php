<?php

namespace Drupal\connectid;

use ConnectId\Api\DataModel\Order;
use ConnectId\Api\DataModel\SubscriptionList;
use ConnectId\Api\LoginApi;
use ConnectId\Api\LoginApiInterface;
use ConnectId\OAuth2\Client\Provider\Exception\InvalidApiResponseException;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Class ApiService.
 */
class UserApiService extends ApiBase implements UserApiServiceInterface {

  /**
   * {@inheritDoc}
   */
  public function getClient(): LoginApiInterface {
    return new LoginApi($this->oauthConfig());
  }

  /**
   * {@inheritDoc}
   */
  public function getOrderFulfillmentUrl(
    Order $order,
    string $successUrl = NULL,
    string $errorUrl = NULL
  ): Url {
    $successReturnUri = $errorReturnUri = \Drupal::request()->getUri();

    if ($successUrl) {
      $successReturnUri = $successUrl;
    }
    if ($errorUrl) {
      $errorReturnUri = $errorUrl;
    }

    $url_string = $this->getClient()->getOrderFulfillmentUrl(
      $order->getOrderId(),
      $successReturnUri,
      $errorReturnUri
    );
    return Url::fromUri($url_string);
  }

  /**
   * {@inheritDoc}
   */
  public function getUserSubscriptions(AccessTokenInterface $access_token): SubscriptionList {
    try {
      $subscriptions = $this->getClient()->getSubscriptionList($access_token);
    }
    catch (InvalidApiResponseException $exception) {
      $this->logger->error('%type: @message in %function (line %line of %file).', Error::decodeException($exception));
      if ($exception->hasAttachedData()) {
        $data = $exception->getAttachedData();
        // User does not exist.
        if ($data['exceptionType'] === 'VALIDATION' && $data['errorMessage'] === 'Customer not found.') {
          return new SubscriptionList();
        }
      }
      // Forward exception
      throw $exception;
    }

    return $subscriptions;
  }

  /**
   * {@inheritDoc}
   */
  public function registerOrder(AccessTokenInterface $access_token, Order $order): Order {
    return $this->getClient()->submitOrder($access_token, $order);
  }

}
