<?php

namespace Drupal\connectid;

use ConnectId\Api\DataModel\Order;
use ConnectId\Api\DataModel\SubscriptionList;
use ConnectId\Api\LoginApiInterface;
use Drupal\Core\Url;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Defines the interface for a ConnectID "Logged-In Mode" API service.
 */
interface UserApiServiceInterface {

  /**
   * Gets a configured instance of the ConnectID client.
   *
   * @return \ConnectId\Api\LoginApiInterface
   */
  public function getClient(): LoginApiInterface;

  /**
   * @param \ConnectId\Api\DataModel\Order $order
   * @param string|NULL $successUrl
   *   Return URL in case of success. Defaults to the current URL.
   * @param string|NULL $errorUrl
   *   Return URL in case of error. Defaults to the current URL.
   *
   * @return \Drupal\Core\Url
   */
  public function getOrderFulfillmentUrl(Order $order, string $successUrl = NULL, string $errorUrl = NULL): Url;

  public function registerOrder(AccessTokenInterface $access_token, Order $order): Order;

  public function getUserSubscriptions(AccessTokenInterface $access_token): SubscriptionList;
}
