<?php

namespace Drupal\connectid\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class DebugController.
 */
class DebugController extends ControllerBase {

  /**
   * Drupal\connectid\ApiService definition.
   *
   * @var \Drupal\connectid\ClientApiServiceInterface
   */
  protected $connectidApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): DebugController {
    $instance = parent::create($container);
    $instance->connectidApi = $container->get('connectid.client_api');
    return $instance;
  }


  public function index(): array {
    $methods = array_diff(get_class_methods(self::class), get_class_methods(parent::class));

    $links = [];
    foreach ($methods as $method) {
      $links[] = [
        '#type'  => 'link',
        '#title' => ucfirst($method),
        '#url'   => Url::fromRoute('connectid.debug_'.$method),
      ];
    }

    return [
      [
        '#theme' => 'item_list',
        '#title' => 'Debug pages',
        '#items' => $links,
      ],
      [
        '#markup' => '<hr>',
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  public function products(): array {
    $build = [$this->index()];
    $products = $this->connectidApi->getProducts();

    $items = [];
    foreach ($products as $item) {
      $items[] = [
        '#prefix' => '<pre>',
        '#markup' => var_export($item->toArray(), TRUE),
        '#suffix' => '</pre>',
      ];
    }
    $build[] =  [
      '#theme' => 'item_list',
      '#title' => 'Products',
      '#items' => $items,
    ];
    if (empty($items)) {
      $build[] = [
        '#type'   => 'markup',
        '#markup' => '<strong>No coupon found for this product.</strong>',
      ];
    }

    return $build;
  }


  public function coupon(Request $request): array {
    $build = [$this->index()];

    $productId = $request->get('product_id');
    if (empty($productId)) {
      $options = [];
      /** @var \ConnectId\Api\DataModel\ProductType $product */
      foreach ($this->connectidApi->getProducts() as $product) {
        $options[$product->getProduct()] = t("@descr (Code: @id)", ['@id'  => $product->getId(), '@descr' => $product->getDescription()]);
      }

      $build[] = [
        'form' => [
          '#type'   => 'form',
          '#method' => 'get',
          'select'  => [
            '#type'    => 'select',
            '#name'    => 'product_id',
            '#title'   => "Select the product: ",
            '#options' => $options,
          ],
          'submit'  => [
            '#type' => 'submit',
          ],
        ],
      ];
    }
    else {
      $c = $this->connectidApi->getProductCoupons($productId);
      $items = [];
      foreach ($c as $item) {
        $items[] = [
          '#prefix' => '<pre>',
          '#markup' => var_export($item->toArray(), TRUE),
          '#suffix' => '</pre>',
        ];
      }
      $build[] =  [
        '#theme' => 'item_list',
        '#title' => 'Coupons',
        '#items' => $items,
      ];
      if (empty($items)) {
        $build[] = [
          '#type'   => 'markup',
          '#markup' => '<strong>No coupon found for this product.</strong>',
        ];
      }
    }

    return $build;
  }


  public function order(Request $request): array {
    $build = [$this->index()];

    $orderId = $request->get('order_id');
    if ($orderId) {
      $order = $this->connectidApi->getOrderStatus($orderId);
      $build['order'] = [
        '#prefix' => '<pre>',
        '#markup' => var_export($order->toArray(), TRUE),
        '#suffix' => '</pre>',
      ];
    }
    else {
      $build[] = [
        'form' => [
          '#type'   => 'form',
          '#method' => 'get',
          'input'   => [
            '#type'  => 'textfield',
            '#name'  => 'order_id',
            '#title' => "Order identifier: ",
          ],
          'submit'  => [
            '#type' => 'submit',
          ],
        ],
      ];
    }

    return $build;
  }
}
