<?php

namespace Drupal\connectid;

use ConnectId\OAuth2\Client\Provider\ConnectId;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use Psr\Log\LoggerInterface;

/**
 * Base class for ConnectID API
 */
abstract class ApiBase {

  /**
   * Drupal "state" storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $stateService;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Drupal configuration service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Drupal module handler service.
   *
   * This is required to check the presence of other optional dependencies.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The current active OAuth configuration.
   *
   * @var array
   */
  protected array $oauthConfig;

  /**
   * Constructs the service object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger service.
   * @param \Drupal\Core\State\StateInterface $state
   *   Drupal State service.
   */
  public function __construct(
    LoggerInterface $logger,
    StateInterface $state,
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler
  ) {
    $this->logger = $logger;
    $this->stateService = $state;
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Loads the correct OAuth configuration for ConnectId.
   *
   * @return array
   *   Kye-Value array with the required OAuth config.
   */
  protected function oauthConfig(): array {
    if (!isset($this->oauthConfig)) {
      $moduleConfig = $this->configFactory->get('connectid.config');
      $this->oauthConfig = [
        'clientId'     => $moduleConfig->get('client_id'),
        'clientSecret' => $moduleConfig->get('client_secret'),
        'testing'      => (bool) $moduleConfig->get('testing_mode'),
      ];

      // Load overrides from the Key module.
      if ($this->moduleHandler->moduleExists('key')) {
        /** @var \Drupal\key\KeyRepositoryInterface $keyRepo */
        $keyRepo = \Drupal::service('key.repository');
        $keyName = $moduleConfig->get('client_secret');
        $this->oauthConfig['clientSecret'] = $keyRepo->getKey($keyName)
          ->getKeyValue();
      }
    }

    return $this->oauthConfig;
  }

  /**
   * Returns a preconfigured OAuth client.
   *
   * @return \ConnectId\OAuth2\Client\Provider\ConnectId
   *   The OAuth client.
   */
  public function oauthClient(): ConnectId {
    return new ConnectId($this->oauthConfig());
  }
}
