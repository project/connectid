<?php

namespace Drupal\connectid;

use ConnectId\Api\ClientApiInterface;
use ConnectId\Api\DataModel\CouponType;
use ConnectId\Api\DataModel\CouponTypeList;
use ConnectId\Api\DataModel\OrderStatus;
use ConnectId\Api\DataModel\ProductType;
use ConnectId\Api\DataModel\ProductTypeList;

/**
 * Defines the interface for a ConnectID "Client Mode" API service.
 */
interface ClientApiServiceInterface {

  /**
   * Gets a configured instance of the ConnectID client.
   *
   * @return \ConnectId\Api\ClientApiInterface
   */
  public function getClient(): ClientApiInterface;

  /**
   * Lists the Products.
   *
   * @see https://mediaconnect-api.redoc.ly/Production/tag/Product#paths/~1v1~1client~1product~1%7BproductType%7D/get
   *
   * @return \ConnectId\Api\DataModel\ProductTypeList
   */
  public function getProducts(): ProductTypeList;

  /**
   * Loads info about one product.
   *
   * @param string $productId
   *   The product identifier.
   *
   * @return \ConnectId\Api\DataModel\ProductType|null
   *@see https://mediaconnect-api.redoc.ly/Production/tag/Product#paths/~1v1~1client~1product~1%7BproductType%7D/get
   */
  public function getProductInfo(string $productId): ?ProductType;

  /**
   * Lists the coupons for a given product.
   *
   * @see https://mediaconnect-api.redoc.ly/Production/tag/Voucher-Coupons
   *
   * @param string $voucherCode
   *   The voucher code for abonement.
   *
   * @return \ConnectId\Api\DataModel\CouponTypeList
   */
  public function getVoucherCoupons(string $voucherCode): CouponTypeList;

  /**
   * Lists the coupons for a given product.
   *
   * @see https://doc.mediaconnect.no/doc/ConnectID/#tag/Coupon/paths/~1v1~1client~1coupon~1{productId}/get
   *
   * @param string $productName
   *
   * @return \ConnectId\Api\DataModel\CouponTypeList
   */
  public function getProductCoupons(string $productName): CouponTypeList;

  /**
   * Loads the info about a specific coupon.
   *
   * @param string $couponId
   *   Coupon identifier
   *
   * @return \ConnectId\Api\DataModel\CouponType|null
   *   Coupon info if found, null if could not load the info.
   */
  public function getCoupon(string $couponId): ?CouponType;

  /**
   * Checks a simple "Status" of an order.
   *
   * @param string $orderId
   *   Order identifier.
   *
   * @return \ConnectId\Api\DataModel\OrderStatus|null
   *   Basic info of an order, or null if could not be loaded.
   */
  public function getOrderStatus(string $orderId): ?OrderStatus;
}
