<?php

namespace Drupal\connectid;

use ConnectId\Api\ClientApi;
use ConnectId\Api\ClientApiInterface;
use ConnectId\Api\DataModel\CouponType;
use ConnectId\Api\DataModel\CouponTypeList;
use ConnectId\Api\DataModel\OrderStatus;
use ConnectId\Api\DataModel\ProductType;
use ConnectId\Api\DataModel\ProductTypeList;
use InvalidArgumentException;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Handles the ConnectID API in "Client Mode".
 */
class ClientApiService extends ApiBase implements ClientApiServiceInterface {

  /** @var \League\OAuth2\Client\Token\AccessTokenInterface */
  protected AccessTokenInterface $clientAccessToken;

  /**
   * @var \ConnectId\Api\DataModel\ProductTypeList
   */
  protected ProductTypeList $productsCache;

  /**
   * @var \ConnectId\Api\DataModel\CouponTypeList[]
   */
  protected array $couponsCache;

  /**
   * {@inheritDoc}
   */
  public function getProducts(bool $reset = FALSE): ProductTypeList {
    if (!isset($this->productsCache) || $reset) {
      $this->productsCache = $this->getClient()->getProductList();
    }

    return $this->productsCache;
  }

  /**
   * {@inheritDoc}
   */
  public function getProductInfo(string $productId): ?ProductType {
    return $this->getClient()->getProductInfo($productId);
  }

  /**
   * {@inheritDoc}
   */
  public function getClient(): ClientApiInterface {
    return new ClientApi($this->oauthConfig());
  }

  /**
   * {@inheritDoc}
   */
  public function getCoupon(string $couponId): ?CouponType {
    [$productCode, $couponNumber] = explode(':', $couponId, 2);
    if (empty($productCode) || empty($couponNumber)) {
      throw new InvalidArgumentException('Invalid coupon id: ' . $couponId);
    }
    return $this->getProductCoupons($productCode)->offsetGet($couponId);
  }

  /**
   * {@inheritDoc}
   */
  public function getVoucherCoupons(string $voucherCode): CouponTypeList {
    if (!isset($this->couponsCache[$voucherCode])) {
      $this->couponsCache[$voucherCode] = $this->getClient()->getCouponListForVoucher($voucherCode);
    }

    return $this->couponsCache[$voucherCode] ?? new CouponTypeList();
  }

  /**
   * {@inheritDoc}
   */
  public function getProductCoupons(string $productName): CouponTypeList {
    if (!isset($this->couponsCache[$productName])) {
      $this->couponsCache[$productName] = $this->getClient()->getCouponListForProduct($productName);
    }

    return $this->couponsCache[$productName] ?? new CouponTypeList();
  }

  /**
   * {@inheritDoc}
   */
  public function getOrderStatus(string $orderId): OrderStatus {
    return $this->getClient()->getOrderStatus($orderId);
  }

}
