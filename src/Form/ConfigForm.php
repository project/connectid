<?php

namespace Drupal\connectid\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('connectid.config');

    $form['client_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Client ID'),
      '#description'   => $this->t(
        'OAuth client identifier (provided by Media Connect)'
      ),
      '#required'      => TRUE,
      '#default_value' => $config->get('client_id'),
    ];

    $form['client_secret'] = [
      '#type'        => 'password',
      '#title'       => $this->t('Client Secret'),
      '#description' => $this->t('OAuth client secret.'),
      // Only required if no value exists in the active configuration.
      '#required'    => empty($config->get('client_secret')),
    ];

    if (\Drupal::moduleHandler()->moduleExists('key')) {
      $form['client_secret']['#type'] = 'key_select';
      $form['client_secret']['#required'] = TRUE;
      $form['client_secret']['#key_filters'] = ['type' => 'connect_id_client_secret'];
      $form['client_secret']['#default_value'] = $config->get('client_secret');
    }
    else {
      $form['client_secret']['#description'] = $this->t(
        'OAuth client secret. <em>Leave empty to use the existing value.</em>'
      );
    }

    $form['testing_mode'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Testing mode'),
      '#description'   => $this->t(
        'Select this if you want to use the testing endpoints (only for development)'
      ),
      '#default_value' => $config->get('testing_mode'),
    ];

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('connectid.config');
    $config->set('testing_mode', $form_state->getValue('testing_mode'))
      ->set('client_id', $form_state->getValue('client_id'));

    // Only update the Client Secret if a new value is provided.
    if (!empty($form_state->getValue('client_secret'))) {
      $config->set('client_secret', $form_state->getValue('client_secret'));
    }

    $config->save();
  }


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'connectid.config',
    ];
  }

}
